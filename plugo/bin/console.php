<?php

require dirname(__DIR__) . '/config/database.php';

$parameters = explode(':', $argv[1]);

class Console{
    private function connect(): PDO {
        $db = new PDO(
            "mysql:host=" . DB_INFOS['host'] . ";port=" . DB_INFOS['port'] . ";dbname=" . DB_INFOS['dbname'],
            DB_INFOS['username'],
            DB_INFOS['password']
        );
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->exec("SET NAMES utf8");
        return $db;
    }

    private function executeQuery(string $query, array $params = []): PDOStatement {
        $db = $this->connect();
        $stmt = $db->prepare($query);
        foreach ($params as $key => $param) $stmt->bindValue($key, $param);
        $stmt->execute();
        return $stmt;
    }

    public function createDataBase($baseName = null){
        if(isset($baseName)){
            $fileBdd = dirname(__DIR__) . '/bin/' . $baseName . '.txt';
            if(file_exists($fileBdd)){
                $myfile = fopen($fileBdd, "r") or die("Unable to open file!");
                $query = fread($myfile,filesize($fileBdd));
                fclose($myfile);
                $this->executeQuery($query);
            }else{
                var_dump('Aucune base de donnée ne porte ce nom.');
                die();
            }
        }else{
            $scandir = scandir("./bin/");
            foreach($scandir as $fichier){
                if ($fichier != '.' && $fichier != '..' && $fichier != 'console.php'){
                    $myfile = fopen(dirname(__DIR__) . '/bin/' . $fichier, "r") or die("Unable to open file!");
                    $query = fread($myfile,filesize(dirname(__DIR__) . '/bin/' . $fichier));
                    fclose($myfile);
                    $this->executeQuery($query);
                }
            }
        }
    }
}


if ($parameters[0] == 'db') {
    switch ($parameters[1]){
        case 'create':
            try {
                if ($parameters[2] === 'database') {
                    $console = new Console();
                    $console->createDataBase();
                }
            } catch (Exception $exception) {
                var_dump($exception);
                die();
            }
            break;
        case 'add':
            try {
                if ($parameters[2] === 'database') {
                    $baseName = $argv[2];
                    $console = new Console();
                    $console->createDataBase($baseName);
                }
            } catch (Exception $exception) {
                var_dump($exception);
                die();
            }
            break;
    }
}
