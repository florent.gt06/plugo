<?php

Namespace App\Controller;

use App\Entity\Article;
use App\Manager\TicketManager;
use Plugo\Controller\AbstractController;

Class MainController extends AbstractController
{

    /**
     * @Routes("/", name="home")
     */
    public function home()
    {
        $articleManager = new TicketManager();
        $this->renderView('home.php', [
            'title' => 'Accueil',
            'articles' => $articleManager->findAll()
        ]);
    }

    /**
     * @Routes("/test", name="test")
     */
    public function test()
    {
        $this->redirectToRoute('home');
    }

    /**
     * @Routes("/about", name="about")
     */
    public function about()
    {
        $this->renderView('about.php', [
            'title' => 'About',
        ]);
    }

}