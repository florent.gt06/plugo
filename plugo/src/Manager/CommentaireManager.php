<?php

namespace App\Manager;

use App\Entity\Commentaire;
use DateTime;
use Plugo\Manager\AbstractManager;

class CommentaireManager extends AbstractManager
{
    public function find(int $id){
        return $this->readOne(Commentaire::class, ['id' => $id]);
    }

    public function findAll() {
        return $this->readMany(Commentaire::class);
    }

    public function add(Commentaire $commentaire) {
        return $this->create(Commentaire::class, [
            'contenu'   => $commentaire->getContenu(),
            'auteur_id' => $commentaire->getAuteur(),
            'ticket_id' => $commentaire->getTicket()
        ]);
    }

    public function edit(Commentaire $commentaire) {
        return $this->update(Commentaire::class, [
            'contenu'   => $commentaire->getContenu(),
            'auteur_id' => $commentaire->getAuteur(),
            'ticket_id' => $commentaire->getTicket(),
            'dateM' =>  new DateTime('now')
        ],
            $commentaire->getId()
        );
    }

    public function remove(Commentaire $commentaire) {
        return $this->delete(Commentaire::class, $commentaire->getId());
    }
}