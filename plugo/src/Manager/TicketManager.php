<?php

namespace App\Manager;

use App\Entity\Ticket;
use DateTime;
use Plugo\Manager\AbstractManager;

class TicketManager extends AbstractManager
{
    public function find(int $id){
        return $this->readOne(Ticket::class, ['id' => $id]);
    }

    public function findAll() {
        return $this->readMany(Ticket::class);
    }

    public function add(Ticket $ticket) {
        return $this->create(Ticket::class, [
            'titre' => $ticket->getTitre(),
            'contenu' => $ticket->getContenu(),
            'auteur_id' => $ticket->getAuteur()
        ]);
    }

    public function edit(Ticket $ticket) {
        return $this->update(Ticket::class, [
            'titre' => $ticket->getTitre(),
            'contenu' => $ticket->getContenu(),
            'dateM' =>  new DateTime('now'),
            'auteur_id' => $ticket->getAuteur()
        ],
            $ticket->getId()
        );
    }

    public function remove(Ticket $ticket) {
        return $this->delete(Ticket::class, $ticket->getId());
    }
}