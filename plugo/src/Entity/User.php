<?php

namespace App\Entity;

use Plugo\Entity\BaseUser;

class User extends BaseUser
{
    private $points;

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points): void
    {
        $this->points = $points;
    }
}