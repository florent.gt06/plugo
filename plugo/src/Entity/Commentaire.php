<?php

namespace App\Entity;

use DateTime;

class Commentaire{
    private $id;
    private $contenu;
    private $dateC;
    private $dateM;
    private $auteur;
    private $ticket;

    public function __construct()
    {
        $this->dateC = new DateTime('now');
        $this->dateM = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     */
    public function setContenu($contenu): void
    {
        $this->contenu = $contenu;
    }

    /**
     * @return mixed
     */
    public function getDateC()
    {
        return $this->dateC;
    }

    /**
     * @param mixed $dateC
     */
    public function setDateC($dateC): void
    {
        $this->dateC = $dateC;
    }

    /**
     * @return mixed
     */
    public function getDateM()
    {
        return $this->dateM;
    }

    /**
     * @param mixed $dateM
     */
    public function setDateM($dateM): void
    {
        $this->dateM = $dateM;
    }

    /**
     * @return mixed
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @param mixed $auteur
     */
    public function setAuteur($auteur): void
    {
        $this->auteur = $auteur;
    }

    /**
     * @return mixed
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param mixed $ticket
     */
    public function setTicket($ticket): void
    {
        $this->ticket = $ticket;
    }
}