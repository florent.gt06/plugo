<h1><?= $datas['title'] ?></h1>

<div class="grid">
    <?php
    if (isset($datas['articles'])){
        foreach ($datas['articles'] as $article){
            ?>
            <article>
                <header><?= $article->getTitle() ?></header>
                <?= $article->getDescription() ?>
                <footer>
                    <a href="<?= path('article.show') . "?id=" . $article->getId() ?>" role="button">Voir l'article</a>
                    <a href="<?= path('article.delete') . "?id=" . $article->getId() ?>" role="button">Supprimer l'article</a>
                    <a href="<?= path('article.edit') . "?id=" . $article->getId() ?>" role="button">Éditer l'article</a>
                </footer>
            </article>
            <?php
        }
    }
    ?>
</div>
