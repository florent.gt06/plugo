<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://unpkg.com/@picocss/pico@latest/css/pico.min.css">
    <title><?= $datas['title']." |" ?? "" ?> Site de démo</title>
</head>
<body>
    <main class="container">
        <?php require '_navbar.php'; ?>
        <?php require $templatePath; ?>
        <?php require '_footer.php'; ?>
    </main>
</body>
</html>