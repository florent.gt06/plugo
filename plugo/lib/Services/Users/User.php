<?php

namespace Plugo\Services\Users;

class User{
    public static function hash($password){
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public static function passwordVerify($password, $hash): bool
    {
        return password_verify($password, $hash);
    }
}