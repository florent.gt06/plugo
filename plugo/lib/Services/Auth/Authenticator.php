<?php

namespace Plugo\Services\Auth;

session_start();
use Plugo\Entity\BaseUser;
use Plugo\Manager\AbstractManager;
use Plugo\Services\Users\User as UserService;


abstract class Authenticator extends AbstractManager {

    const STATUS_CONNECTED = 0;
    const STATUS_LOGOUT = 1;
    const STATUS_INVALID_CREDENTIALS = 2;
    const STATUS_SIGN_IN_FAILED = 3;
    const STATUS_USER_CREATED = 4;

    /**
     * @param $login
     * Login -> ce qui permet à l'utilisateur de se connecter (par défault email) [Doit être une colonne unique]
     * @param $password
     * @param string $uniqueSearch
     * uniqueSearch permet de choisir la colonne à chercher (En rapport avec le login) [Doit être une colonne unique]
     * @return array
     */
    private function login($login, $password, string $uniqueSearch = 'email'): array
    {
        $canLogin = false;
        $user = $this->readOne(BaseUser::class, [
            $uniqueSearch =>  $login
        ]);
        if ($user instanceof BaseUser){
            if (UserService::passwordVerify($password, $user->getPassword())){
                $canLogin = true;
            }
        }

        if ($canLogin){
            $_SESSION['user'] = serialize($user);
            return [
                'status'    =>  self::STATUS_CONNECTED,
                'message'   =>  'Utilisateur ' . $user->getFullNameUpper() . 'connecté.',
                'user'      =>  $user
            ];
        }

        return [
            'status'    =>  self::STATUS_INVALID_CREDENTIALS,
            'message'   =>  'Identifiants invalides.',
        ];
    }

    /**
     * @param $datas
     * @return array
     */
    private function signIn($datas): array
    {
        if (!isset($datas['email']) || !isset($datas['username']) || !isset($datas['firstname']) || !isset($datas['lastname']) || !isset($datas['password'])){
            return [
                'status'    =>  self::STATUS_SIGN_IN_FAILED,
                'message'   =>  'Veuillez renseigner tous les champs obligatoires.'
            ];
        }
        $user = new BaseUser();
        $user->setEmail($datas['email']);
        $user->setUsername($datas['username']);
        $user->setFirstname($datas['firstname']);
        $user->setLastname($datas['lastname']);
        $user->setPassword(UserService::hash($datas['password']));
        if (isset($datas['roles'])){
            $user->setRoles(implode('|', $datas['roles']));
        }else{
            $user->setRoles('ROLE_USER');
        }

        return [
            'status'    =>  self::STATUS_USER_CREATED,
            'message'   =>  'Utilisateur ' . $user->getFullNameUpper() . 'créé.',
            'user'      =>  $user
        ];
    }

    /**
     * Permet de se déconnecter
     * @return array
     */
    private function logout(): array
    {
        session_destroy();
        return [
            'status'    =>  self::STATUS_LOGOUT,
            'Utilisateur déconnecté.'
        ];
    }

    /**
     * Permet de connecter un utilisateur
     * @param $user
     * @return array
     */
    private function forceLogin($user): array
    {
        $_SESSION['user'] = serialize($user);
        return [
            'status'    =>  self::STATUS_CONNECTED,
            'message'   =>  'Utilisateur ' . $user->getFullNameUpper() . 'connecté.',
            'user'      =>  $user
        ];
    }

}