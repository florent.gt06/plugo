<?php

namespace Plugo\Services\Security;

class Security {
    public static function canAccess($rolesAccess): bool
    {
        $user = unserialize($_SESSION['user']);
        $keys = array_keys(ROLES, $rolesAccess);
        foreach ($keys as $role){
            if (in_array($role, $user->getRoles())){
                return true;
            }
        }
        return false;
    }
}