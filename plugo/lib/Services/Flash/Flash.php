<?php

namespace Plugo\Services\Flash;

class Flash{
    const SUCCESS = "success";
    const ERROR = "error";

    public static function flash($type, $message){
        $_SESSION['flash'] = [
            'type'  =>  $type,
            'message'   =>  $message,
        ];
    }
}