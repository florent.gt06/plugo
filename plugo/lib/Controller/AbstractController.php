<?php

namespace Plugo\Controller;

use App\Controller\Erreur;

abstract class AbstractController {

    protected function renderView(string $template, array $datas = []): string {
        $templatePath = dirname(__DIR__, 2) . '/template/pages/' . $template;
        return require_once dirname(__DIR__, 2) . '/template/layout.php';
    }

    protected function redirectToRoute(string $name, array $params = []): void
    {
        $arr = array_filter(ROUTES, function($ar) use ($name) {
            return ($ar['name'] == $name);
        });

        if (!empty($arr)){
            $uri = array_keys($arr)[0];
        }else{
            Throw new \Exception('Aucune route trouvé.');
        }


        if (!empty($params)) {
            $strParams = [];
            foreach ($params as $key => $val) {
                array_push($strParams, urlencode((string) $key) . '=' . urlencode((string) $val));
            }
            $uri .= '?' . implode('&', $strParams);
        }

        header("Location: " . $uri);
        die;
    }
}
