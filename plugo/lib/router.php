<?php

use App\Controller\Erreur;

require dirname(__DIR__) . '/config/routes.php';

$availableRouteNames = array_keys(ROUTES);

if (isset($_GET['page'])) {
    if (in_array($_GET['page'], $availableRouteNames)){
        $route = ROUTES[$_GET['page']];
        $controller = new $route['controller'];
        $controller->{$route['method']}();
    }else{
        //Erreur 404
        $controller = new Erreur();
        $controller->notFound();
    }
}

function path($name){
    $arr = array_filter(ROUTES, function($ar) use ($name) {
        return ($ar['name'] == $name);
    });
    return array_keys($arr)[0];
}

