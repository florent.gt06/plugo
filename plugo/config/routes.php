<?php

const ROUTES = [
    'home' => [
        'controller' => App\Controller\MainController::class,
        'method' => 'home',
        'name' => 'home'
    ],
    '' => [
        'controller' => App\Controller\MainController::class,
        'method' => 'home',
        'name' => 'home'
    ],
    'about' => [
        'controller' => App\Controller\MainController::class,
        'method' => 'about',
        'name' => 'about'
    ],
];